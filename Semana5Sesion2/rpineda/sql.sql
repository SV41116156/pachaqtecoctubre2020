

-- -----------------------------------------------------
-- Table pachaqtec.pais
-- -----------------------------------------------------
CREATE TABLE  pachaqtec.pais (
  idpais INT NOT NULL,
  nombre VARCHAR(45) NOT NULL,
  PRIMARY KEY (idpais));



-- -----------------------------------------------------
-- Table pachaqtec.curso
-- -----------------------------------------------------
CREATE TABLE  pachaqtec.curso (
  idcurso INT NOT NULL,
  nombre VARCHAR(45) NOT NULL,
  PRIMARY KEY (idcurso)
  );



-- -----------------------------------------------------
-- Table pachaqtec.alumno
-- -----------------------------------------------------
CREATE TABLE  pachaqtec.alumno (
  idalumno INT NOT NULL,
  nombre VARCHAR(45) NULL,
  apellido_paterno VARCHAR(45) NOT NULL,
  apellido_materno VARCHAR(45) NOT NULL,
  edad INT NOT NULL,
  idpais INT NOT NULL,
  PRIMARY KEY (idalumno, idpais),

  CONSTRAINT fk_alumno_pais1
    FOREIGN KEY (idpais)
    REFERENCES pachaqtec.pais (idpais)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);



-- -----------------------------------------------------
-- Table pachaqtec.matricula
-- -----------------------------------------------------
CREATE TABLE  pachaqtec.matricula (
  idalumno INT NOT NULL,
  idcurso INT NOT NULL,
  PRIMARY KEY (idalumno, idcurso),

  CONSTRAINT fk_alumno_has_curso_alumno1
    FOREIGN KEY (idalumno)
    REFERENCES pachaqtec.alumno (idalumno)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_alumno_has_curso_curso1
    FOREIGN KEY (idcurso)
    REFERENCES pachaqtec.curso (idcurso)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

alter table pachaqtec.curso
   add constraint UQ_alumnos_documento
   unique (idcurso);


alter table pachaqtec.alumno
    add constraint UQ_alumno
   unique (idalumno);


insert into pachaqtec.curso(idcurso, nombre)
values(1, 'Programacion'),
(2, 'Diseño');

insert into pachaqtec.pais(idpais, nombre)
values(1, 'Peru'),
(2, 'Ecuador');

insert into pachaqtec.alumno(
    idalumno ,
  nombre ,
  apellido_paterno ,
  apellido_materno ,
  edad ,
  idpais 
)
values(3, 'Karen', 'Lam', 'Serquen', 24, 1);

insert into pachaqtec.matricula(idalumno,idcurso)
values(1,1),
(1,2),
(2,2),
(3,1);

update pachaqtec.alumno
set nombre = 'Roberto'
where idalumno = 1;
