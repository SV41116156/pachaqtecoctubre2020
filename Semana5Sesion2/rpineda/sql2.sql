CREATE DATABASE colegio ;

 USE colegio ;



CREATE TABLE pais (

 id INT PRIMARY KEY,

 nombre VARCHAR(45) NOT NULL);



CREATE TABLE alumno (

 id INT PRIMARY KEY,

 nombre VARCHAR(45) NOT NULL,

 apellido_paterno VARCHAR(80) NOT NULL,

 apellido_materno VARCHAR(80) NOT NULL,

 edad INT NOT NULL,

 pais_id INT NOT NULL,

 FOREIGN KEY (pais_id) REFERENCES pais(id)

 );



CREATE TABLE curso (

 curso_id INT PRIMARY KEY,

 nombre VARCHAR(45) NOT NULL

 );



CREATE TABLE matricula (

 alumno_id INT NOT NULL,

 curso_id INT NOT NULL,

 PRIMARY KEY (alumno_id, curso_id),

 FOREIGN KEY (alumno_id) REFERENCES alumno (id),

 FOREIGN KEY (curso_id) REFERENCES curso (curso_id)

 );