"""
Esta es una calculadora sencilla
"""
import funcionesAritmeticas

print("""
=====================================
     Bienvenido a mi Calculadora
=====================================     
""")
print("Escribe la operación a realizar: ")
operacion = input().lower()
print("Escogiste la operacion :"+ operacion)
print("Escribe el primer número:")
numero1 = int(input())
print("Escribe el segundo número:")
numero2 = int(input())

if(operacion == "suma"):
    respuesta = funcionesAritmeticas.Suma(numero1,numero2)
    print(respuesta)
else:
    print("Operacion "+ operacion + "No Implementada ")