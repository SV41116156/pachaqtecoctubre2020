"""
Este Módulo es para definir las operaciones Aritméticas
"""
def Suma(numero1, numero2):
    resultado = numero1 + numero2
    return resultado

def Resta(numero1, numero2):
    resultado = numero1 - numero2
    return resultado

def Multiplicacion(numero1, numero2):
    resultado = numero1 * numero2
    return resultado

def Division(numero1, numero2):
    resultado = numero1 / numero2
    return resultado

def DivisionEntera(numero1, numero2):
    resultado = numero1 // numero2
    return resultado

def Modulo(numero1, numero2):
    resultado = numero1 % numero2
    return resultado

def Potencia(numero1, numero2):
    resultado = numero1 ** numero2
    return resultado