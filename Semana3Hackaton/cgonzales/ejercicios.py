import math


def EdadPeso ():
    try:
        edad = int (input ('Ingrese su Edad (años): '))
        peso = int (input ('Ingrese su peso (Kg.): '))
        print (f'Usted tiene {edad} años y su peso es: {peso} kilos')
    except ValueError:
        print ('Solo valores numericos!')

    
def AreaTriangulo ():
    try:
        base = int (input ('Indique cuanto mide la base del triangulo (metros): '))
        altura = int (input ('Indique cuanto mide la altura del triangulo (metros): '))
        area = base * altura * 0.5
        print (f'El area del Triangulo cuyo base y altura son {base} y {altura} respectivamente es: {area} metros cuadrados')
    except ValueError:
        print ('Solo valores numericos!')


def AreaCirculo ():
    try:
        radio = int (input (f'Ingrese cuanto mide el radio (m) del Circulo '))
        area = math.pi * radio ** 2
        print (f'El Area del circulo de radio {radio} metros es {area} metros cuadrados')
    except ValueError:
        print ('Solo valores numericos!')


def DividirMayor ():
    try:
        a = int (input ('Ingrese el primer valor '))
        b = int (input ('Ingrese el segundo valor '))
        if a >= b:
            if b != 0:
                print (f'La divion sera: {a/b}')
            else:
                print ('El Denominador no puede ser cero')
        else:
            if a != 0:
                print (f'La divion sera: {b/a}')
            else:
                print ('El Denominador no puede ser cero')
    except ValueError:
        print ('Solo valores numericos!')

def MayorDeTres ():
    try:
        a = int (input ('Ingrese el primer numero '))
        b = int (input ('Ingrese el segundo numero '))
        c = int (input ('Ingrese el tercer numero '))
        numeros = [a,b,c]
        mayor = numeros[0]
        for numero in numeros:
            if numero > mayor:
                mayor = numero
        print (f'El numero Mayor es: {mayor}')
    except ValueError:
        print ('Solo valores numericos!')


def FormatoFecha ():
    try:
        dia = int (input ('Ingrese el dia '))
        mes = int (input ('Ingrese el mes '))
        año = int (input ('Ingrese el año '))
        mesStr = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 
                'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre',
                'Diciembre']
        mesDias = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        if dia <= 0:
            print ('Error, El numero de dias no puede ser menor de cero')
        if dia > mesDias[mes-1]: 
            print (f'Error, El mes de {mesStr[mes-1]} no tiene mas de {mesDias[mes-1]} dias')
        elif mes > 12 and mes < 1:
            print (f'Error No existe el mes {mes}')
        elif año < 0:
            print (f'Error No existe el año negativo')
        else:
            print (f'{dia} dias de {mesStr[mes-1]} del año {año}' )
    except ValueError:
        print ('Solo valores numericos!')


def PuedeVotar ():
    try:
        edad = int (input ('Ingrese su edad actual '))
        sexo = int (input ('Ingrese (1) si eres Hombre o (2) si eres Mujer '))
        if sexo == 1 or sexo == 2:
            if edad > 17:
                if sexo == 1:
                    print ('Eres Hombre y puedes Votar')
                else:
                    print ('Eres Mujer y puedes Votar')
            elif edad < 18 and edad > -1:
                if sexo == 1:
                    print ('Eres Hombre y no puedes Votar')
                else:
                    print ('Eres Mujer y no puedes Votar')
            else:
                print ('Edad incorrecta')
        else:
            print ('Ingresa 1 o 2 la proxima vez...')
    except ValueError:
        print ('Solo valores numericos!')


def CalcularSueldo ():
    try:
        horas = int (input ('Ingrese el numero de horas que laboro el ultimo mes '))
        sueldoHora = int (input ('Ingrese su remuneracion x hora de trabajo '))
        sueldo = 0
        if horas >= 35:
            sueldo = (horas - 35) * sueldoHora * 1.5 + 35 * sueldoHora
        else:
            sueldo =  horas * sueldoHora
        if sueldo > 20000:
            print (f'Su sueldo actual es: {sueldo*.8}')
        else:
            print (f'Su sueldo actual es: {sueldo}')
    except ValueError:
        print ('Solo valores numericos!')


def NumerosPrimos ():
    numeros = 1
    primo = 0
    while (numeros <=100 ):
        for contador in range (1, numeros + 1, 1):
            if numeros % contador == 0:
                primo = primo + 1
        if (primo <= 2):
            print (f'{numeros}')
        primo = 0
        numeros = numeros + 1


def CalcularFactorial ():
    try:
        numero = int (input ('Ingrese el numero que quiere calcular su factorial '))
        factorial = 1
        for contador in range (1, numero + 1, 1):
            factorial = factorial * contador
        print (f'El Factorial de {numero} es: {factorial}')
    except ValueError:
        print ('Solo valores numericos!')


def TablasMultiplicar ():
    try:
        numero = int (input ('Ingrese el numero para mostrar su tabla de multiplicar '))
        contador = 1
        for contador in range (0, 11, 1):
            print (f'{contador} X {numero} = {contador * numero}')
    except ValueError:
        print ('Solo valores numericos!')


def MaximoMinimoMedia ():
    try:
        numeros = int (input ('Ingrese el primer numero '))
        maximo = numeros
        minimo = numeros
        media = 0
        lista = []
        while numeros != 0:
            lista.append (numeros)
            numeros = int (input ('Ingrese el siguiente numero '))
        for numero in lista:
            if numero < minimo:
                minimo = numero
            if numero > maximo:
                maximo = numero
            media = media + numero
        print (f'El {maximo} es el valor maximo, {minimo} es el valor minimo y {media/len (lista)} es la media de los valores ingresados')
    except ValueError:
        print ('Solo valores numericos!')


def SecuenciaNumeros ():
    try:
        numeros = int (input ('Ingrese el primer numero '))
        mediaAritmetica = 0
        lista = []
        while numeros != -1:
            lista.append (numeros)
            numeros = int (input ('Ingrese el siguiente numero '))
        for numero in lista:
            mediaAritmetica = mediaAritmetica + numero
        print (f'La media aritmedica de la secuencia de numeros ingresados es: {mediaAritmetica / len (lista)}')
    except ValueError:
        print ('Solo valores numericos!')


def VoucherCliente ():
    try:
        importe = int (input ('Ingrese el importe de consumo '))
        print ('Marque el (1) si el mes es Enero')
        print ('Marque el (2) si el mes es Febrero')
        print ('Marque el (3) si el mes es Marzo')
        print ('Marque el (4) si el mes es Abril')
        print ('Marque el (5) si el mes es Mayo')
        print ('Marque el (6) si el mes es Junio')
        print ('Marque el (7) si el mes es Julio')
        print ('Marque el (8) si el mes es Agosto')
        print ('Marque el (9) si el mes es Setiembre')
        print ('Marque el (10) si el mes es Octubre')
        print ('Marque el (11) si el mes es Noviembre')
        print ('Marque el (12) si el mes es Diciembre')
        mes = int (input ('Ingrese el mes en que realizo su compra '))
        if mes == 10:
            print (f'El importe a cobrar es {importe*.85}')
        else:
            print (f'El importe a cobrar es {importe}')
    except ValueError:
        print ('Solo valores numericos!')

def EvaluarDiez ():
    try:
        numeros = int (input ('Ingrese el primer numero '))
        contador = 1
        lista = [numeros]
        suma = 0
        pares = 0
        promedioImpares = 0
        while contador < 10:
            numeros  = int (input ('Ingrese el siguiente numero '))
            lista.append (numeros)
            contador = contador + 1
        for numero in lista:
            if numero % 2 == 0:
                pares = pares + 1
            else:
                promedioImpares = promedioImpares + numero
            suma = suma + numero
        print (f'Se ingresaron {pares} numeros pares, La suma de los numeros es {suma} y el promedio de los numeros impares es {promedioImpares/(10-pares)}')
    except ValueError:
        print ('Solo valores numericos!')