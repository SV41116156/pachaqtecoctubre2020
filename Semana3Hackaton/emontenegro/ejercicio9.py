'''
Escribir un método que encuentre y despliegue los números primos entre uno y cien. Un número primo es divisible entre el mismo y la unidad por lo tanto un numero primo no puede ser par excepto el dos (2).
'''

def esNumeroPrimo (num):
    if num > 1:
        for i in range(2, num):
            if (num % i) == 0:
                return False
            else:
                return True

try:
    for i in range(1, 100):
        if esNumeroPrimo(i):
            print(f"{i} Es Primo")

except Exception as error:
    print(f"Se ha encontrado un error: {error}")