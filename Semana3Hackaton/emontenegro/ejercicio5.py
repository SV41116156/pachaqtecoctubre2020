'''
Escribir un método que lea de entrada 3 números y que indique cual es el mayor de ellos.
'''

# Import math Library
import math

while True:
    try:
        num1 = float(input("Digitar el numero 1: "))
        num2 = float(input("Digitar el numero 2: "))
        num3 = float(input("Digitar el numero 3: "))
        
        if num1 > num2 and num1 > num3:
            mayor = num1
        elif num2 > num1 and num2 > num3:
            mayor = num2
        else:
            mayor = num3

        print(f"El mayor de los 3 numeros es: {mayor}")
        break
    except ValueError:
        print(f"Ingresar valores validos (Numeros)")
    except Exception as error:
        print(f"Se ha encontrado un error: {error}")


        