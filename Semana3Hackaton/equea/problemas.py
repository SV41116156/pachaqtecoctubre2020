'''
Metodos para resolver los problemas
'''
'''
Escribir un método de un programa que permita 
leer la edad y peso de una persona y posteriormente imprimirla
'''
def problema1(edad, peso):
    return f"Tu edad es {edad} y tu peso es {peso}"
'''
Escribir un metodo que calcule el área de un 
triángulo recibiendo como entrada el valor de base y altura
'''
def problema2(base, altura):
    return f"El area del triangulo es {base*altura/2} "
'''
Escribir metodo que calcule el área de un círculo.
'''
def problema3(radio):
    area = radio * radio * 3.1416
    return f"El area del circulo es {area} "
'''
Escribir metodo que dados 2 valores de entrada 
imprima siempre la división del mayor entre el menor.
'''
def problema4(PrimerNumero, SegundoNumero):
    if (PrimerNumero > SegundoNumero):
	    total = PrimerNumero / SegundoNumero
    else: 	
	    total = SegundoNumero / PrimerNumero
    return f"La division es {total} "           
'''
Escribir metodo que lea de entrada 3 números y 
que indique cual es el mayor de ellos.
'''
def problema5(PrimerNumero, SegundoNumero, TercerNumero):
    if (PrimerNumero == SegundoNumero) and (SegundoNumero == TercerNumero):   
        A = "Numeros iguales"
    else:
        if (PrimerNumero > SegundoNumero) and (PrimerNumero > TercerNumero):
        	A = PrimerNumero
        else:
            if (SegundoNumero > PrimerNumero) and (SegundoNumero > TercerNumero): 
        	    A = SegundoNumero
            else:
           	    A = TercerNumero
    return f"El numero mayor es {A} "        
# '''
# Escribir un metodo que lea 3 números los cuales 
# significan una fecha (día, mes, año). Comprobar que sea válida la fecha, 
# si no es valido que imprima un mensaje de error, y si es válida imprimir 
# el mes con su nombre.
# '''
def problema6(dia, mes, año):
    dia >= 1
    dia < 32
    mes >= 1
    mes < 13
    año >= 1
    if (mes == 1):  
       B = "Enero"
    if (mes == 2):
       B = "Febrero"                
    if (mes == 3):                 
       B = "Marzo"
    if (mes == 4):
       B = "Abril"                  
    if (mes == 5):                        
       B = "Mayo" 
    if (mes == 6):
       B = "Junio"                                                     
    if (mes == 7):
       B = "Julio"                                                            
    if (mes == 8):                            
       B = "Agosto"                            
    if (mes == 9):                             
       B = "Setiembre"                                
    if (mes == 10):                                      
       B = "Octubre"                                  
    if (mes == 11):                                        
       B = "Noviembre"                                       
    if (mes == 12):                                     
       B = "Diciembre"                                            
    return f"La fecha es {dia} de {B} de {año} " 
# '''
# Escribir un metodo que pida la edad y el sexo 
# y dependiendo si es hombre o mujer y si puede votar o no.
# '''
def problema7(edad, sexo):
    if (edad >= 18):
        A = " que tiene edad para votar"
    else:
        A = " que aun es menor de edad"
    return f"usted es un/una {sexo} {A} " 
# '''
# Realice un Pseudocódigo que calcule la nómina salarial neto, 
# de unos obreros cuyo trabajo se paga en horas. El cálculo se realiza de la siguiente forma:
# Condiciones
# Las primeras 35 horas a una tarifa fija.
# Las horas extras se pagan a 1.5 más de la tarifa fija.
# Las horas extras se pagan a 1.5 más de la tarifa fija.
# Los impuestos a deducir de los trabajadores varían, según el 
# sueldo mensual si el sueldo es menos a S./ 20,000.00 el sueldo es 
# libre de impuesto y si es al contrario se cobrará un 20% de impuesto.
# '''
def problema8(horas, pago):
    C = horas * pago
    D = horas * pago * 0.8
    E = horas - 35
    F = (horas + E * 1.5) * pago
    G = F * 0.8
    if (horas <= 35) and (pago < 571.4):
        total = C
    else:
        if (horas <= 35) and (pago > 571.4):
            total = D
        else:
            if (horas > 35) and (F < 20000):
                total = F
            else:
                total = G
    return f"Tu sueldo neto es de {total} "             
# '''
# Escribir un metodo que encuentre y despliegue los números 
# primos entre uno y cien. Un número primo es divisible entre el mismo 
# y la unidad por lo tanto un numero primo no puede ser par excepto el dos (2).
# '''
def problema9(A):
    A = int
    B = int
    C = int
    A = 1
    while ( A <= 100):
        B = 1
        C = 0
        while ( B <= A):
            if (A % B == 0):
                C = C+1
            B = B+1
        if (C == 2):
            print(A)
        A = A+1
    return f"  {print} "     
 # '''
# Hacer un metodo que calcule el factorial de un número.
# '''
def problema10(numero):
    numero > 0
    C = 1
    B = 1
    while C <= numero:
        B = B * C
        C = C + 1
    return f" El factorial es {B} "  
# '''
# Hacer un metodo que despliegue las tablas de multiplicar.
# '''
def problema11(numero):
    B = int
    for B in range (0,11):
        print( B, "X" , numero, " = " , (B*numero))   
    return f"  {print} "        
# '''
# Algoritmo que lea números enteros hasta teclear 0, y nos muestre el 
# máximo, el mínimo y la media de todos ellos. Piensa como debemos inicializar 
# las variables.
# '''
def problema12(numero):
    numero = int(input("introduzca un numeros hasta teclear 0 "))
    minimo = numero
    maximo = numero
    suma = 0
    contador = 1
    while (numero != 0):
        if (numero > maximo):
            maximo = numero
        if (numero < minimo):
            minimo = numero
        suma = suma + numero
        contador = contador + 1
        numero = int(input("introduzca un numeros hasta teclear 0 "))
    media = suma / (contador)
    print("el maximo es", maximo)
    print("el minimo es", minimo)
    print(" la media es", media)
    return f"  {print} " 
# '''
# Dada una secuencia de números leídos por teclado, que acabe con un –1, 
# por ejemplo: 5,3,0,2,4,4,0,0,2,3,6,0,……,-1; Realizar el algoritmo que 
# calcule la media aritmética. Suponemos que el usuario no insertara numero 
# negativos.
# '''
def problema13(numero):
    numero = int(input("introduzca un numeros hasta teclear -1 "))
    suma = 0
    contador = 1
    while (numero != - 1):
        suma = suma + numero
        contador = contador + 1
        numero = int(input("introduzca un numeros hasta teclear -1 "))
    return f"la media aritmetica {suma/(contador+1)}"
# '''
# Una tienda ofrece un descuento del 15% sobre el total de la compra 
# durante el mes de octubre. Dado un mes y un importe, calcular cuál 
# es la cantidad que se debe cobrar al cliente.
# '''
def problema14(importe, mes):
    if (mes == "octubre" ):
        C = importe * 0.85
    else:
        C = importe
    return f"el total es {C}"
# Dados 10 números enteros que se ingresan por teclado, calcular 
# cuántos de ellos son pares, cuánto suman ellos y el promedio de 
# los impares
def problema15(numero):
    numero = 1
    B = int
    suma = int
    sumapar = int
    sumaimp = int
    while numero <= 10:
        B 
        suma = suma + B
        if (B % 2 == 0):
            sumapar = sumapar + B
        else:
            sumapar = sumaimp + B
        numero = numero +1
    print(" Suma total" , suma)
    print(" Suma de los pares" , sumapar)
    print(" Suma de los impares" , sumaimp)
    return f"el total es {print}"
<<<<<<< HEAD
=======








>>>>>>> develop
