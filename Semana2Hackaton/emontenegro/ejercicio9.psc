SubProceso res <- esNumeroPrimo ( num )
	res = Verdadero
	
	Si (num <= 1) Entonces
		res = Falso;
	SiNo
		Si (num = 2) Entonces
			res = Verdadero;
		SiNo
			Si (num mod 2 = 0) Entonces
				res = Falso;
			FinSi
		FinSi
	FinSi
	Para i Desde 3 Hasta rc(num) Con Paso 2 Hacer
		Si (num mod i == 0) Entonces
			res = Falso;
		FinSi
	Fin Para
	
Fin SubProceso


Proceso ejercicio9
	Escribir "Mostrar numeros primos entre 1 y 100"
	
	Para i<-1 Hasta 100 Con Paso 1 Hacer
		Si esNumeroPrimo(i) Entonces
			Escribir i," Es Primo"
		FinSi
	Fin Para
	
FinProceso
