class Cliente:
    def __init__(self, idcliente, nombre, apellido):
        self.idcliente = idcliente
        self.nombre = nombre
        self.apellido = apellido
    
class Mesero:
    def __init__(self, idmesero, nombre, apellido):
        self.idmesero = idmesero
        self.nombre = nombre
        self.apellido = apellido

class Menu:
    def __init__(self, idmenu, nombre, valor):
        self.idmenu = idmenu
        self.nombre = nombre
        self.valor = valor

class Mesa:
    def __init__(self, idmesa, numero):
        self.idmesa = idmesa
        self.numer = numero

class Orden:
    def __init__(self, idorden, fecha, idcliente, idmesero, idmesa):
        self.idorden = idorden
        self.fecha = fecha
        self.idcliente = idcliente
        self.idmesero = idmesero
        self.idmesa = idmesa

class OrdenDetalle:
    def __init__(self,idordendetalle, idorden, idmenu, cantidad):
        self.idordendetalle = idordendetalle
        self.idorden = idorden
        self.idmenu = idmenu
        self.cantidad = cantidad