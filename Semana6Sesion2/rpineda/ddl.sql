-- -----------------------------------------------------
-- Table cliente
-- -----------------------------------------------------
CREATE TABLE  cliente (
  idcliente serial NOT NULL ,
  nombre VARCHAR(45) NOT NULL,
  apellido VARCHAR(45) NOT NULL,
  PRIMARY KEY (idcliente));



-- -----------------------------------------------------
-- Table mesero
-- -----------------------------------------------------
CREATE TABLE  mesero (
  idmesero serial NOT NULL,
  nombre VARCHAR(45) NOT NULL,
  apellido VARCHAR(45) NOT NULL,
  PRIMARY KEY (idmesero));



-- -----------------------------------------------------
-- Table menu
-- -----------------------------------------------------
CREATE TABLE  menu (
  idmenu serial NOT NULL,
  nombre VARCHAR(45) NOT NULL,
  valor DECIMAL NOT NULL,
  PRIMARY KEY (idmenu));



-- -----------------------------------------------------
-- Table mesa
-- -----------------------------------------------------
CREATE TABLE  mesa (
  idmesa serial NOT NULL,
  numero INT NOT NULL,
  PRIMARY KEY (idmesa));


-- -----------------------------------------------------
-- Table ordencabecera
-- -----------------------------------------------------
CREATE TABLE  ordencabecera (
  idordencabecera serial NOT NULL,
  fecha DATE NOT NULL,
  idcliente INT NOT NULL,
  idmesero INT NOT NULL,
  idmesa INT NOT NULL,
  PRIMARY KEY (idordencabecera),
  CONSTRAINT fk_ordencabecera_cliente
    FOREIGN KEY (idcliente)
    REFERENCES cliente (idcliente)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_ordencabecera_mesero1
    FOREIGN KEY (idmesero)
    REFERENCES mesero (idmesero)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_ordencabecera_mesa1
    FOREIGN KEY (idmesa)
    REFERENCES mesa (idmesa)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);



-- -----------------------------------------------------
-- Table ordendetalle
-- -----------------------------------------------------
CREATE TABLE  ordendetalle (
  idordendetalle serial NOT NULL,
  idordencabecera INT NOT NULL,
  idmenu INT NOT NULL,
  cantidad int not null,
  PRIMARY KEY (idordendetalle),
  CONSTRAINT fk_ordendetalle_menu1
    FOREIGN KEY (idmenu)
    REFERENCES menu (idmenu)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_ordendetalle_ordencabecera1
    FOREIGN KEY (idordencabecera)
    REFERENCES ordencabecera (idordencabecera)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
